use std::io::{Stdout, Write, stdout};

pub struct Timer<T: Write>
{
    now: instant::Instant,
    out: T
}

impl<T: Write> From<T> for Timer<T>
{
    fn from(out: T) -> Self
    {
        Timer { now: instant::Instant::now(), out }
    }
}

impl<T: Write> Timer<T>
{
    pub fn stdout() -> Timer<Stdout>
    {
        Timer::from(stdout())
    }

    pub fn reset(&mut self)
    {
        self.now = instant::Instant::now();
    }

    pub fn elapsed(&self) -> std::time::Duration
    {
        self.now.elapsed()
    }

    pub fn println(&mut self, text: &str) -> std::io::Result<()>
    {
        write!(self.out, "{}: {}\n", text, self.now.elapsed().as_millis())
    }
}
